package br.ucsal.testequalidade20162.domain;

import java.util.ArrayList;
import java.util.List;

public class Condutor {

	private Integer numeroCnh;

	private String nome;

	private List<Multa> multas = new ArrayList<>();

	public Condutor(Integer numeroCnh, String nome) {
		super();
		this.numeroCnh = numeroCnh;
		this.nome = nome;
	}

	public Integer getNumeroCnh() {
		return numeroCnh;
	}

	public void setNumeroCnh(Integer numeroCnh) {
		this.numeroCnh = numeroCnh;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void incluirMulta(Multa multa) {
		multas.add(multa);
	}

	public List<Multa> getMultas() {
		return new ArrayList<>(multas);
	}

	@Override
	public String toString() {
		return "Condutor [numeroCnh=" + numeroCnh + ", nome=" + nome + ", multas=" + multas + "]";
	}

}
